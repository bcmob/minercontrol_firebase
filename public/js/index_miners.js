function miners() {
    // // 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥
    // // The Firebase SDK is initialized and available here!
    //
    // firebase.auth().signInWithEmailAndPassword("email@gmail.com", "password")

    // firebase.auth().onAuthStateChanged(user => { 
    //   console.log(user)
    // });
    const dateoptions = {
      // year: '2-digit',
      month: '2-digit',
      day: '2-digit',
      timezone: 'UTC',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric'
    };

    const dtable = new webix.ui({
      container: "box",
      id: 'datarigs',
      view: "datatable",
      autoheight: true,
      autowidth: true,
      // autoConfig:true,
      columns: [
        { id: "name", header: "Name", sort: "string" },
        { id: "ver", header: "Version" },
        { id: "uptime", header: "Uptime" },
        { id: "hashrate", header: "Hashrate" },
        { id: "tempsfans", header: "Temps/Fans", width: 550 },
        { id: "lastinfo", header: "LastInfo", width: 120 }
      ]
    });

    function parseTempsFansClaymore(str) {
      let tf = str.split(';')
      let strout = ''
      for (var i = 0; i < tf.length; i++) {
        if (i % 2 == 0)
          strout += '<strong>' + ((parseInt(tf[i]) >= 80) ? '<font color="red">' + tf[i] + '</font>' : tf[i]) + '°</strong>'
        else {
          if (tf[i].indexOf("W") != -1)
            strout += `:${tf[i]} `
          else
            strout += (tf.length != 16) ? `:${tf[i]}% ` : '  ' //скрываем обороты для Р106

        }
      }
      return strout
    }

    function updateTable(rigSnap) {
      let item = $$('datarigs').getItem(rigSnap.val().name)
      const record = {
        id: rigSnap.val().name,
        name: rigSnap.val().name,
        ver: rigSnap.val().ver,
        uptime: rigSnap.val().uptime,
        hashrate: (rigSnap.val().hashrate / 1000.0).toFixed(3) + ' Mh/s',
        tempsfans: parseTempsFansClaymore(rigSnap.val().tempsfans),
        lastinfo: (new Date(rigSnap.val().timestamp)).toLocaleString("ru", dateoptions),
        timestamp: rigSnap.val().timestamp
      }

      if (!item) {
        dtable.add(record);
      } else {
        dtable.updateItem(rigSnap.val().name, record);
      }
    }

    firebase.database().ref().once('value', snapshotRigs => {
      snapshotRigs.forEach(rigSnap => {
        // console.log('%s', rigSnap.key);
        if (!rigSnap.val().name) return;
        // console.log(rigSnap.val());
        updateTable(rigSnap);
      });
      checkRigs();
    });

    firebase.database().ref().on('child_changed', rigSnap => {
      // console.log('%s', rigSnap.key);
      if (!rigSnap.val().name) return;
      console.log(rigSnap.val());
      updateTable(rigSnap);
    });

    function checkRigs() {
      fetch('https://blockchain.info/ticker?cors=true')
        .then(res => res.json())
        .then((out) => {
          const rate_usd_btc = Math.round(out.USD["15m"]);
          document.getElementById('rate').innerHTML = `1 BTC = ${rate_usd_btc} $`;
        })
        .catch(err => console.error(err));

      const count = dtable.count()
      for (var i = 0; i < count; i++) {
        const id = dtable.getIdByIndex(i)
        const item = dtable.getItem(id)

        const temps = item.tempsfans.match(/\d{2}°/g)
        const tempAverage = temps ? temps.reduce(function(a, b) { return parseInt(a) + parseInt(b); }) / temps.length : 0
        // console.log(tempAverage)

        if (Date.now() - item.timestamp > 60000 || item.ver == 'Error') {
          webix.message({ type: "error", text: item.name + " не отвечает более 2мин!", expire: 10000 })
          if (dtable.hasCss(id, "checkedlight")) dtable.removeRowCss(id, "checkedlight")
          if (dtable.hasCss(id, "highlight")) dtable.removeRowCss(id, "highlight")
          if (!dtable.hasCss(id, "warninglight")) dtable.addRowCss(id, "warninglight")
          // console.log(item.name + " Ошибка, время " + (Date.now() - item.timestamp))
        } else if (tempAverage < 30) {
          if (dtable.hasCss(id, "warninglight")) dtable.removeRowCss(id, "warninglight")
          if (dtable.hasCss(id, "checkedlight")) dtable.removeRowCss(id, "checkedlight")
          if (!dtable.hasCss(id, "highlight")) dtable.addRowCss(id, "highlight")

        } else {
          if (dtable.hasCss(id, "warninglight")) dtable.removeRowCss(id, "warninglight")
          if (dtable.hasCss(id, "highlight")) dtable.removeRowCss(id, "highlight")
          if (!dtable.hasCss(id, "checkedlight")) dtable.addRowCss(id, "checkedlight")
          // console.log(item.name + " Норм, время " + (Date.now() - item.timestamp))
        }

      }
    }

    setInterval(checkRigs, 60000)

    try {
      let app = firebase.app();
      let features = ['auth', 'database', 'messaging', 'storage'].filter(feature => typeof app[feature] === 'function');
      document.getElementById('load').innerHTML = `Firebase SDK loaded with ${features.join(', ')}`;
    } catch (e) {
      console.error(e);
      document.getElementById('load').innerHTML = 'Error loading the Firebase SDK, check the console.';
    }
  }