const fetch = require('node-fetch');
const firebase = require("firebase");

const rigName = 'RigTest';
const rigAddrClaymore = 'http://127.0.0.1:3333';
const rigAddrEWBF = 'http://127.0.0.1:42000/getstat';

let failureClaymore = false;
let failureEWBF = false;

const config = {
    apiKey: " AIzaSyDn6b-4IuPxi16JdNW8Cj96XIXCmZR3pr8",
    authDomain: "claymoredualminermonitor.firebaseapp.com",
    databaseURL: "https://claymoredualminermonitor.firebaseio.com"
};
firebase.initializeApp(config);
// firebase.auth().signInAnonymously()
function autorization() {
    firebase.auth().signInWithEmailAndPassword("email1@gmail.com", "password")
    .catch(error => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
            console.error('Wrong password.');
        } else {
            console.error(errorMessage);
        }
        console.log(error);
    });
    
}

autorization();

const rootRef = firebase.database().ref().child(rigName);

function parseClaymore(api) {
    console.log(new Date().toISOString() + ' => ' + parseClaymoreForLog(api.result));
    const data = api.result;
    rootRef.set({
        name: rigName,
        ver: data[0],
        uptime: data[1] + 'min',
        hashrate: parseInt(data[2].split(';')[0]),
        tempsfans: data[6],
        timestamp: firebase.database.ServerValue.TIMESTAMP
    }).catch(error => {
        // const errorCode = error.code;
        console.error(error.message);
        autorization();
    });

    function parseClaymoreForLog(result) {
        return result[0] + ' ' + result[1] + 'min ' + result[2].split(';')[0] + 'h/s ' + result[6];
    }
}

function parseEWBF(api) {
    console.log(new Date().toISOString() + ' => ' + parseEWBFforLog(api));

    let hashrate = 0;
    let tempsfans = "0;0";
  
    let tempTF = []
    api.result.forEach(element => {
        hashrate += element.speed_sps
        tempTF.push(element.temperature, `${element.gpu_power_usage}W`)
    });
    tempsfans = tempTF.join(";");

    rootRef.set({
        name: rigName,
        ver: api ? 'EWBF' : 'Error',
        uptime: api ? Math.round((Date.now() - (api.start_time * 1000)) / 60000) + 'min' : '-',
        hashrate: hashrate,
        tempsfans: tempsfans,
        timestamp: firebase.database.ServerValue.TIMESTAMP
    }).catch(error => {
        console.error(error.message);
        autorization();
    });


    function parseEWBFforLog(result) {
        return "EWBF" + ' ' + Math.round((Date.now() - (result.start_time * 1000)) / 60000) + 'min ' + ' current_server:' + result.current_server + ' ' + result.server_status;
    }
    
}

setInterval(() => {
    if (!failureClaymore){
        fetch(rigAddrClaymore)
            .then(res => res.text())
            .then(text => {
                const api = JSON.parse(text.match(/{"result".*}/i)[0])
                parseClaymore(api)
            })
            .catch(err => {
                console.warn(err.message)
                failureClaymore = true
            })
    } else if (!failureEWBF)
        fetch(rigAddrEWBF)
            .then(res => res.json())
            .then(json => parseEWBF(json))
            .catch(err => {
                console.warn(err.message)
                failureEWBF = true
            })
    else {
        failureClaymore = false;
        failureEWBF = false;
    }
}, 30000);