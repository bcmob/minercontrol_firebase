function pools() {
    
    const RIGS = [ 
        { name: 'etc-ua', url:'https://194.28.86.24:8001/'}, 
        { name: 'etc-eu', url:'https://85.17.194.191:8001/'}
    ];
    let last_block = []
    let last_block_old = []

    const dview = new webix.ui({
        view: 'dataview',
        id: 'pools',
        container: 'box_pools',
        css: 'pools',
        type:{
            width: 535,
            height: 230,
            template:"<div class='overall'><div class='name'>#name#</div><div class='block'>#old_block#</div><div class='block'>#first_block#</div><div class='last_block'>#last_block#</div><div class='miners'>#miners#</div><div class='balance'>#balance#</div><div class='load'>#load#</div><div class='log'>#log#</div></div>"
        },
        autoheight: true
    })

    RIGS.forEach((item,i) =>{dview.add({name:item.name, url:item.url},0)})

    setInterval(checkPools, 15000)

    function checkPools() {
        const count = dview.count()
        for (let i = 0; i < count; i++) {
          const id = dview.getIdByIndex(i)
          const item = dview.getItem(id)
  
          console.log(item)

          fetch(item.url)
            .then(res => res.json())
            .then((api) => {
                console.log(api)
                let block_mine, first_block, miners, geth_log;

                // item.name = item.name

                try {
                    block_mine = api.block_mine[api.block_mine.length-1];
                    first_block = api.block_mine[0].match(/(\d{7,8})/)[0];
                    last_block[i] = block_mine.match(/(\d{7,8})/)[0];
                } catch (err) {
                    // console.log('Monitor parse api.block_mine error -> ', api.block_mine);
                    block_mine = 'error';
                    first_block = 'error';
                    last_block[i] = 'error';
                }

                item.old_block = `oldblock: ${last_block_old[i]}`
                item.first_block = `first_block: ${first_block}`
                item.last_block = `last_block: ${last_block[i]}`
                try {
                    item.miners = `miners: ${api.miners.match(/(\d{1,4})/)[0]}`;
                } catch (err) {
                // console.log('Monitor parse api.miners error -> ', api.miners);
                    item.miners = 'error';
                }
                item.balance = `balance ${api.balance}`

                let loadavg1m, loadavg5m, loadavg15m;
                try {
                    const apiloadavg = api.loadavg;
                    loadavg1m = Math.round(apiloadavg[0] * 100) / 100;
                    loadavg5m = Math.round(apiloadavg[1] * 100) / 100;
                    loadavg15m = Math.round(apiloadavg[2] * 100) / 100;
                } catch (err) {
                    console.log('Monitor parse api.loadavg error -> ', api.loadavg);
                    loadavg1m = 'error';
                    loadavg5m = 'error';
                    loadavg15m = 'error';
                }
                item.load = `loadavg: ${loadavg1m} ${loadavg5m} ${loadavg15m} freemem: ${Math.round( api.freemem / 1e6)} MB`
                item.log = api.geth_log

                dview.refresh();
                //or
                // dview.updateItem("book2", item2);
                last_block_old[i] = last_block[i]
            })
            .catch(err => {
                console.error(err)
                item.old_block = ''
                item.first_block = ''
                item.last_block = ''
                item.miners = ''
                item.balance = 'error'
                item.load = ''
                item.log = ''
                dview.refresh();
            });
  
        }
      }
  
    // $$("pools").clearAll();
}
